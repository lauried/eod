textures/effects/hdr_inside
{
	qer_editorimage textures/effects/hdr_in
	qer_trans 0.3
	surfaceparm trans
	{
		map textures/d/888.tga
		blendFunc GL_ONE GL_ONE
	}
}

textures/effects/hdr_outside
{
	qer_editorimage textures/effects/hdr_out
	qer_trans 0.3
	surfaceparm trans
	{
		map textures/d/aaa.tga
		blendFunc GL_ZERO GL_SRC_COLOR
	}
}

