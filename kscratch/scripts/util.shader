textures/util/origin
{
	qer_editorimage textures/util/origin
	qer_trans 0.5
    surfaceparm origin
}

textures/util/clip
{
	qer_editorimage textures/util/clip
	qer_trans 0.3
	surfaceparm trans
}

textures/util/trigger
{
	qer_editorimage textures/util/trigger
	qer_trans 0.3
}

textures/util/skybox
{
    qer_editorimage textures/util/skybox
    qer_trans 0.75
    surfaceparm sky
}

