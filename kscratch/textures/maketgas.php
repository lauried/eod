<?php

$colourLevels = [
	0 => '0',
	1 => '1',
	2 => '2',
	3 => '3',
	4 => '4',
	5 => '5',
	6 => '6',
	7 => '7',
	8 => '8',
	9 => '9',
	10 => 'a',
	11 => 'b',
	12 => 'c',
	13 => 'd',
	14 => 'e',
	15 => 'f',
];

$data = [
	0, // identsize
	0, // colourmap none
	2, // file type rgb
	0, 0, // colourmapstart
	0, 0, // colourmaplength
	0, // colourmapbits
	0, 0, // x origin
	0, 0, // y origin
	1, 0, // width (LE?)
	1, 0, // height (LE?)
	24, // bits per pixel
	0, // image descriptor bits (VH flip)
];

function MakeFile($r, $g, $b, $nameTemplate)
{
	global $data;
	global $colourLevels;
	
	$step = 255.0 / count($colourLevels);

    $rgb = (string)$colourLevels[$r] . (string)$colourLevels[$g] . (string)$colourLevels[$b];
    $filename = str_replace('{RGB}', $rgb, $nameTemplate);

	$fp = fopen($filename, 'wb');
	foreach ($data as $d) {
		fwrite($fp, chr($d));
	}
	fwrite($fp, chr($b * $step));
	fwrite($fp, chr($g * $step));
	fwrite($fp, chr($r * $step));
	fclose($fp);
}

mkdir('d');
mkdir('u');
$size = count($colourLevels);
for ($r = 0; $r < $size; $r += 1)
{
	for ($g = 0; $g < $size; $g += 1)
	{
		for ($b = 0; $b < $size; $b += 1)
		{
			MakeFile($r, $g, $b, 'd/{RGB}.tga');
            MakeFile($r, $g, $b, 'u/unshaded{RGB}.tga');
		}
	}
}

?>
