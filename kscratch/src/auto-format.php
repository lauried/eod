<?php

$dir = new DirectoryIterator(dirname(__FILE__));
foreach ($dir as $fileinfo) {
	if ($fileinfo->isDot()) {
		continue;
	}
	if ($fileinfo->getExtension() !== "qc") {
		continue;
	}

	$filename = $fileinfo->getFilename();

	echo "{$filename}\n";
	// 1. convert tabs at start to 4 spaces
	// 2. convert remaining tabs (those at the end) to 8 spaces (as original tab alignment used 8)
	// 3. convert spaces at start to tabs, width 4
	$output = shell_exec("expand --initial --tabs=4 {$filename} | expand --tabs=8 | unexpand --first-only --tabs=4");
	file_put_contents($filename, $output);
}
