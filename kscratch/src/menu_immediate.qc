/*
-------------------------------------------------------------------------------
Immediate mode menu

We let the user code be responsible for changing what menu is available - this
simplifies immediate-mode menus themselves.
For our purposes we can just swap back and forth between menus, but it's
possible to manage a stack of menus and their current state.

Usage:
Implement a MenuFunc that calls menu_action, menu_cvar on the entity it's given.
-------------------------------------------------------------------------------
*/

#define MENU_ACTIVATE 1
#define MENU_NEXT 2
#define MENU_PREV 3
#define MENU_INCREMENT 4
#define MENU_DECREMENT 5
#define MENU_EXIT 6

#define CVAR_INCREMENT 4
#define CVAR_DECREMENT 5
#define CVAR_DISPLAY 7

#define BIND_KEY 8

typedef void(entity funcs) MenuFunc;
typedef string(string cvar, float action) CvarHandler;

var MenuFunc immediate_current_menu;
float immediate_selected_item;

// Our injected functions use this to tell what item they're at.
vector immediate_counter; // x is max width, y is max height, z is num items
float immediate_current_action;
vector immediate_draw_mins;
vector immediate_draw_maxs;
vector immediate_draw_size;
vector immediate_draw_pos;
// This is for catching keybinds
float immediate_capturing_bind;
float immediate_captured_key;

.void(string name, void() command) menu_action;
.void(string name, string cvar_name, CvarHandler handler) menu_cvar;
.void(string name, string bind) menu_bind;
.void(string label) menu_spacer;

MenuFunc immediate_NullMenu = {};

//-----------------------------------------------------------------------------
// Interface
//-----------------------------------------------------------------------------

void(MenuFunc menu, float selected_item) Immediate_SetMenu =
{
	immediate_current_menu = menu;
	immediate_selected_item = selected_item;
};

void(vector origin, vector size) Immediate_DrawMenu =
{
	// TODO: Count the menu items, and if they won't fit, then we have to
	// offset the menu so it's outside the bounds.
	// We should offset it so that we stay at least 3 from the bottom, unless
	// we're in the last two.

	immediate_draw_mins = origin;
	immediate_draw_maxs = origin + size;
	immediate_draw_pos = origin;
	immediate_draw_size = size;

	entity e = spawn();
	immediate_SetupDraw(e);
	immediate_current_menu(e);
	remove(e);
};

float(float keynum, float ascii) Immediate_KeyDown =
{
	if (immediate_capturing_bind)
	{
		immediate_capturing_bind = FALSE;
		if (keynum != K_ESCAPE)
		{
			immediate_captured_key = keynum;
			Immediate_HandleAction(BIND_KEY);
		}
		return TRUE;
	}

	if (keynum == K_UPARROW)
	{
		Immediate_HandleAction(MENU_PREV);
		return TRUE;
	}
	else if (keynum == K_DOWNARROW)
	{
		Immediate_HandleAction(MENU_NEXT);
		return TRUE;
	}
	else if (keynum == K_ENTER)
	{
		Immediate_HandleAction(MENU_ACTIVATE);
		return TRUE;
	}
	else if (keynum == K_RIGHTARROW)
	{
		Immediate_HandleAction(MENU_INCREMENT);
		return TRUE;
	}
	else if (keynum == K_LEFTARROW)
	{
		Immediate_HandleAction(MENU_DECREMENT);
		return TRUE;
	}

	return FALSE;
};

void(float action) Immediate_HandleAction =
{
	vector count;
	if (action == MENU_NEXT)    
	{
		immediate_selected_item = immediate_selected_item + 1;
		count = immediate_CountItems();
		if (immediate_selected_item >= count_z)
		{
			immediate_selected_item = 0;
		}
		return;
	}
	if (action == MENU_PREV)
	{
		immediate_selected_item = immediate_selected_item - 1;
		count = immediate_CountItems();
		if (immediate_selected_item < 0)
		{
			immediate_selected_item = count_z - 1;
		}
		return;
	}

	immediate_current_action = action;

	entity e = spawn();
	immediate_SetupHandle(e);
	immediate_current_menu(e);
	remove(e);
};

vector() Immediate_CountItems =
{
	return immediate_CountItems();
};

vector() immediate_CountItems =
{
	entity e = spawn();
	immediate_SetupCount(e);
	immediate_current_menu(e);
	remove(e);
	return immediate_counter;
};

//-----------------------------------------------------------------------------
// Drawing
//-----------------------------------------------------------------------------

// TODO: Different drawing styles.

void(entity e) immediate_SetupDraw =
{
	immediate_counter_z = -1;
	e.menu_action = immediate_Draw_Action;
	e.menu_cvar = immediate_Draw_Cvar;
	e.menu_bind = immediate_Draw_Bind;
	e.menu_spacer = immediate_Draw_Spacer;
};

void(string name, void() command) immediate_Draw_Action =
{
	float selected;
	vector size;

	immediate_counter_z += 1;

	if (immediate_draw_pos_y + 8 >= immediate_draw_maxs_y)
	{
		return;
	}

	selected = (immediate_counter_z == immediate_selected_item);
	size = immediate_draw_size;
	size_y = 8;
	Draw_MenuAction(immediate_draw_pos, size, name, selected);

	immediate_draw_pos_y += 8;
};

void(string name, string cvar_name, CvarHandler handler) immediate_Draw_Cvar =
{
	float selected;
	vector size;

	immediate_counter_z += 1;

	if (immediate_draw_pos_y + 16 >= immediate_draw_maxs_y)
	{
		return;
	}

	selected = (immediate_counter_z == immediate_selected_item);
	size = immediate_draw_size;
	size_y = 16;
	Draw_MenuCvar(immediate_draw_pos, size, name, handler(cvar_name, CVAR_DISPLAY), selected);

	immediate_draw_pos_y += 16;
};

void(string name, string bind) immediate_Draw_Bind =
{
	float selected;
	vector size;

	immediate_counter_z += 1;

	if (immediate_draw_pos_y + 8 >= immediate_draw_maxs_y)
	{
		return;
	}

	selected = (immediate_counter_z == immediate_selected_item);
	size = immediate_draw_size;
	size_y = 8;

	if (selected && immediate_capturing_bind)
	{
		selected = 2;
	}

	Draw_MenuBind(immediate_draw_pos, size, name, bind, selected);

	immediate_draw_pos_y += 8;
};

void(string label) immediate_Draw_Spacer =
{
	vector size;

	if (immediate_draw_pos_y + 8 >= immediate_draw_maxs_y)
	{
		return;
	}

	size = immediate_draw_size;
	size_y = 8;
	Draw_MenuAction(immediate_draw_pos, size, label, FALSE);
	
	immediate_draw_pos_y += 8;
};


//-----------------------------------------------------------------------------
// Handling action
//-----------------------------------------------------------------------------

void(entity e) immediate_SetupHandle =
{
	immediate_counter_z = -1;
	e.menu_action = immediate_Handle_Action;
	e.menu_cvar = immediate_Handle_Cvar;
	e.menu_bind = immediate_Handle_Bind;
	e.menu_spacer = immediate_Handle_Spacer;
};

void(string name, void() command) immediate_Handle_Action =
{
	immediate_counter_z += 1;
	if (immediate_counter_z == immediate_selected_item && immediate_current_action == MENU_ACTIVATE)
	{
		command();
	}
};

void(string name, string cvar_name, CvarHandler handler) immediate_Handle_Cvar =
{
	immediate_counter_z += 1;
	if (immediate_counter_z == immediate_selected_item)
	{
		handler(cvar_name, immediate_current_action);
	}
};

void(string name, string bind) immediate_Handle_Bind =
{
	immediate_counter_z += 1;
	if (immediate_counter_z == immediate_selected_item)
	{
		if (immediate_current_action == BIND_KEY)
		{
			KeyBinds_ChangeBoundKey(immediate_captured_key, bind);
		}
		else if (immediate_current_action == MENU_ACTIVATE)
		{
			immediate_capturing_bind = TRUE;
		}
	}
};

void(string label) immediate_Handle_Spacer =
{
};

//-----------------------------------------------------------------------------
// Calculating submenu
//-----------------------------------------------------------------------------

void(entity e) immediate_SetupCount =
{
	immediate_counter = '0 0 0';
	e.menu_action = immediate_Count_Action;
	e.menu_cvar = immediate_Count_Cvar;
	e.menu_bind = immediate_Count_Bind;
	e.menu_spacer = immediate_Count_Spacer;
};

void(string name, void() command) immediate_Count_Action =
{
	immediate_counter_z += 1;
	immediate_counter_y += 8;
};

void(string name, string cvar_name, CvarHandler handler) immediate_Count_Cvar =
{
	immediate_counter_z += 1;
	immediate_counter_y += 16;
};

void(string name, string bind) immediate_Count_Bind =
{
	immediate_counter_z += 1;
	immediate_counter_y += 8;
};

void(string label) immediate_Count_Spacer =
{
	immediate_counter_y += 8;
};

