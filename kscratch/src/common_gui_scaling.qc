/*
-------------------------------------------------------------------------------
GUI Scaling
Lets us create a low resolution display by scaling by an integer amount, and
automatically picks the highest scaling in which a given number of 8x8
characters will fit on screen.
Draw functions are wrapped so that we can do everything in the virtual
resolution.
-------------------------------------------------------------------------------
*/

// Translates from our virtual pixellated resolution to screen/window.
vector vid_scale;
// Translates from our virtual pixellated resolution to console space,
// for draw calls which are affected by conwidth/conheight.
vector gui_scale;
// Size of the screen in our virtual pixellated resolution.
vector gui_screen_size;

// Safe area that there'll always be room for on screen, in virtual pixel space.
vector gui_safe_min;
vector gui_safe_size;

// Define a kind of 'character grid' area
vector gui_grid_min; // offset of the grid in pixels
vector gui_grid_size; // size of the grid area in pixels
vector gui_cell_size; // size of the grid area in cells

#define GUI_SCALE(vec) vec ## _x = vec ## _x * gui_scale_x; vec ## _y = vec ## _y * gui_scale_y;
#define VID_SCALE(vec) vec ## _x = vec ## _x * vid_scale_x; vec ## _y = vec ## _y * vid_scale_y;

void(float desired_width, float desired_height, vector vid_size) GuiScaling_Init =
{
	float scale;
	float conwidth, conheight;
	float max_xscale, max_yscale;

	// TODO: Optionally set up the cell grid parameters to exactly fit desired_width and
	// desired_height, with any border.

	conwidth = cvar("vid_conwidth");
	conheight = cvar("vid_conheight");

	for (max_xscale = 1; desired_width * (max_xscale + 1) <= vid_size_x; max_xscale = max_xscale + 1) {}
	for (max_yscale = 1; desired_height * (max_yscale + 1) <= vid_size_y; max_yscale = max_yscale + 1) {}

	scale = max_xscale;
	if (max_xscale > max_yscale)
	{
		scale = max_yscale;
	}

	vid_scale_x = scale;
	vid_scale_y = scale;

	// Scaling factor has to take into account that all our draws are adjusted
	// by conwidth/conheight.
	gui_scale_x = scale * (conwidth / vid_size_x);
	gui_scale_y = scale * (conheight / vid_size_y);

	gui_screen_size_x = conwidth / gui_scale_x;
	gui_screen_size_y = conheight / gui_scale_y;

	if (0)
	{
		// Scale the grid to fill the screen
		gui_grid_size_x = floor(gui_screen_size_x / 8) * 8;
		gui_grid_size_y = floor(gui_screen_size_y / 8) * 8;
	}
	else
	{
		// Have the grid be the size of the desired area
		gui_grid_size_x = floor(desired_width / 8) * 8;
		gui_grid_size_y = floor(desired_height / 8) * 8;
	}

	gui_grid_min = (gui_screen_size - gui_grid_size) * 0.5;
	gui_grid_min_x = floor(gui_grid_min_x);
	gui_grid_min_y = floor(gui_grid_min_y);

	gui_cell_size = gui_grid_size / 8;

	// Safe region, including offset to centre it.
	gui_safe_size = vec2(desired_width, desired_height);
	gui_safe_min = (gui_screen_size - gui_safe_size) * 0.5;
	gui_safe_min_x = floor(gui_safe_min_x);
	gui_safe_min_y = floor(gui_safe_min_y);
};

vector(vector pixel_coord) GuiScaling_PixelToCell =
{
	return (pixel_coord - gui_grid_min) / 8;
};

vector(vector cell_coord) GuiScaling_CellToGrid =
{
	return gui_grid_min + cell_coord * 8;
};

vector(vector cell_coord) GuiScaling_CellSizeToGrid =
{
	return cell_coord * 8;
};

vector(vector real_coord) GuiScaling_RealToVirtual =
{
	real_coord_x /= vid_scale_x;
	real_coord_y /= vid_scale_y;
	return real_coord;
};

#ifdef CSQC
void(vector min, vector size) GuiScaling_SetViewport =
{
	VID_SCALE(min)
	VID_SCALE(size)
	R_SetView(VF_MIN, min);
	R_SetView(VF_SIZE, size);
};
#endif

void(float width, vector pos1, vector pos2, vector rgb, float alpha, float flags) drawline =
{
	GUI_SCALE(pos1)
	GUI_SCALE(pos2)
	return _drawline(width * gui_scale_x, pos1, pos2, rgb, alpha, flags);
};

float(vector position, float character, vector scale, vector rgb, float alpha, float flag) drawcharacter =
{
	GUI_SCALE(position)
	GUI_SCALE(scale)
	return _drawcharacter(position, character, scale, rgb, alpha, flag);
};

float(vector position, string text, vector scale, vector rgb, float alpha, float flag) drawstring =
{
	GUI_SCALE(position)
	GUI_SCALE(scale)
	return _drawstring(position, text, scale, rgb, alpha, flag);
};

float(vector position, string pic, vector size, vector rgb, float alpha, float flag) drawpic =
{
	GUI_SCALE(position)
	GUI_SCALE(size)
	return _drawpic(position, pic, size, rgb, alpha, flag);
};

float(vector position, vector size, vector rgb, float alpha, float flag) drawfill =
{
	GUI_SCALE(position)
	GUI_SCALE(size)
	return _drawfill(position, size, rgb, alpha, flag);
};

void(float x, float y, float width, float height) drawsetcliparea =
{
	return _drawsetcliparea(x * gui_scale_x, y * gui_scale_y, width * gui_scale_x, height * gui_scale_y);
};

float(vector position, vector size, string pic, vector srcPosition, vector srcSize, vector rgb, float alpha, float flag) drawsubpic =
{
	GUI_SCALE(position)
	GUI_SCALE(size)
	return _drawsubpic(position, size, pic, srcPosition, srcSize, rgb, alpha, flag);
};

vector() Gui_GetMousePos = 
{
	vector v = Cursor_GetPos();
	v_x = floor(v_x / gui_scale_x);
	v_y = floor(v_y / gui_scale_y);
	return v;
}

void(vector v) Gui_SetMousePos =
{
	v_x = v_x * gui_scale_x;
	v_y = v_y * gui_scale_y;
	Cursor_SetPos(v);
};

void(vector org, vector texcoords, vector rgb, float alpha) R_PolygonVertex =
{
	GUI_SCALE(org)
	_R_PolygonVertex(org, texcoords, rgb, alpha);
};
