/*
-------------------------------------------------------------------------------
Paper
-------------------------------------------------------------------------------
*/

#define THE_WORST_HANDLE_EVER 0

#define PAPER_COLS 80
#define PAPER_ROWS 24

.float paper_buffer;
.float paper_page;
.float paper_num_pages;
.float paper_mode;
.string paper_filename;

//-----------------------------------------------------------------------------
// External stuff
//-----------------------------------------------------------------------------

void() Paper_Init =
{
	entity e = System_Register();
	e.on_gamestate_change = paper_ChangeGameState;
	e.on_console_command = paper_ConsoleCommand;
	e.on_sync = paper_OnSync;
};

void(entity player, float mode, string filename) Paper_Show =
{
	paper_SetPlayerPaper(player, mode, filename);
	paper_SendPlayerState(player, TRUE);
	player.fixmove = TRUE;
};

void(entity player) Paper_Clear =
{
	paper_ClearPlayerBuffer(player);
	paper_SendPlayerState(player, FALSE);
	player.fixmove = FALSE;
};

void(entity player) Paper_NextPage =
{
	player.paper_page = player.paper_page + 1;
	if (player.paper_page >= player.paper_num_pages)
	{
		player.paper_page = player.paper_num_pages - 1;
	}
	paper_SendPlayerState(player, FALSE);
};

void(entity player) Paper_PrevPage =
{
	player.paper_page = player.paper_page - 1;
	if (player.paper_page < 0)
	{
		player.paper_page = 0;
	}
	paper_SendPlayerState(player, FALSE);
};

//-----------------------------------------------------------------------------
// Network Model
//-----------------------------------------------------------------------------

void(entity player, float first_page) paper_SendPlayerState =
{
	float rows, cols, flags;
	float start, end, p, rowscale, colscale, cellrows, cellcols;
	string font;

	if (player.paper_buffer < 0)
	{
		Popup_Close(player);
		return;
	}

	rows = paper_RowsForMode(player.paper_mode);
	cols = paper_ColsForMode(player.paper_mode);
	font = paper_FontForMode(player.paper_mode);
	rowscale = 1;
	colscale = 1;
	if (font == "tall")
	{
		rowscale = 2;
	}
	if (font == "small")
	{
		colscale = 0.5;
	}
	cellrows = rows * rowscale + 2;
	cellcols = cols * colscale;
	p = Popup_Start(cellcols, cellrows);
	Popup_SetBackground(p, "eee");
	Popup_SetForeground(p, "000");
	Popup_SetFont(p, font);
	start = player.paper_page * rows;
	end = min(buf_getsize(player.paper_buffer), (player.paper_page + 1) * rows);
	for (float i = start; i < end; i += 1)
	{
		Popup_AddText(p, bufstr_get(player.paper_buffer, i));
		Popup_AddNewLine(p);
	}

	Popup_SetFont(p, "large");

	Popup_SetPos(p, vec2(cellcols - 1, cellrows - 1));
	flags = POPUP_FLAGS_AUTOCLOSE;
	if (first_page)
	{
		flags = flags | POPUP_FLAGS_DEFAULT;
	}
	Popup_AddButton(p, chr2str(31), POPUP_STYLE_BUTTON, "cmd sv_paper_close", flags);

	if (player.paper_page > 0)
	{
		Popup_SetPos(p, vec2(cellcols - 3, cellrows - 1));
		Popup_AddButton(p, chr2str(29), POPUP_STYLE_BUTTON, "cmd sv_paper_prev", 0);
	}
	if (player.paper_page < player.paper_num_pages - 1)
	{
		Popup_SetPos(p, vec2(cellcols - 5, cellrows - 1));
		Popup_AddButton(p, chr2str(30), POPUP_STYLE_BUTTON, "cmd sv_paper_next", 0);
	}

	Popup_Send(p, player);
	Popup_Delete(p);
};

void(entity player) paper_OnSync =
{
	// We have to reload the buffer if it's not already there.
	if (player.paper_filename && player.paper_filename != "")
	{
		player.paper_buffer = paper_LoadToBuffer(player.paper_filename);
		paper_SendPlayerState(player, TRUE);
	}
};

//-----------------------------------------------------------------------------
// Data Model
//-----------------------------------------------------------------------------

void(entity player, float mode, string filename) paper_SetPlayerPaper =
{
	float rows = paper_RowsForMode(mode);

	paper_ClearPlayerBuffer(player);
	player.paper_buffer = paper_LoadToBuffer(filename);
	if (player.paper_buffer <= 0)
	{
		return;
	}
	player.paper_filename = filename;
	player.paper_mode = mode;
	player.paper_page = 0;
	player.paper_num_pages = ceil(buf_getsize(player.paper_buffer) / rows);
	if (player.paper_num_pages < 1)
	{
		player.paper_num_pages = 1;
	}
};

void(entity player) paper_ClearPlayerBuffer =
{
	if (player.paper_buffer >= 0)
	{
		buf_del(player.paper_buffer);
		player.paper_buffer = -1;
	}
	player.paper_filename = "";
};

float(string filename) paper_LoadToBuffer =
{
	float buf;

	if (substring(filename, 0, 1) != "/")
	{
		filename = strcat("text/", world.model, filename);
	}
	else
	{
		filename = strcat("text", filename);
	}

	buf = buf_create();
	// If we created buffer 0, discard it. 0 is an awkward value to use when everything's zero-initialised.
	if (buf == THE_WORST_HANDLE_EVER)
	{
		buf = buf_create();
		buf_del(THE_WORST_HANDLE_EVER);
	}

	if (buf_loadfile(filename, buf))
	{
		return buf;
	}
	buf_del(buf);
	localcmd("echo \"Failed to load file: ");
	localcmd(filename);
	localcmd("\"\n");
	return -1;
};

//-----------------------------------------------------------------------------
// Command Handling
//-----------------------------------------------------------------------------

float(float state) paper_ChangeGameState =
{
	return TRUE;
};

float(string line) paper_ConsoleCommand =
{
	if (line == "sv_paper_next")
	{
		Paper_NextPage(self);
		return TRUE;
	}
	if (line == "sv_paper_prev")
	{
		Paper_PrevPage(self);
		return TRUE;
	}
	if (line == "sv_paper_close")
	{
		Paper_Clear(self);
		return TRUE;
	}
	return FALSE;
};

