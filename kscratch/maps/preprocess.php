<?php

// Preprocess a .map file to adjust its colours to simulate lighting.

// $argv[0] is the php file name, $argv[1] is the map name, $argv[2] is the destination.

function subtract($a, $b)
{
    return [ $a[0] - $b[0], $a[1] - $b[1], $a[2] - $b[2] ];
}

function dotproduct($a, $b)
{
    return $a[0] * $b[0] + $a[1] * $b[1] + $a[2] * $b[2];
}

function crossproduct($a, $b)
{
    return [
        $a[1] * $b[2] - $a[2] * $b[1],
        $a[2] * $b[0] - $a[0] * $b[2],
        $a[0] * $b[1] - $a[1] * $b[0],
    ];
}

function normalize($p)
{
    $scale = $p[0] * $p[0] + $p[1] * $p[1] + $p[2] * $p[2];
    $scale = 1.0 / sqrt($scale);

    return [ $p[0] * $scale, $p[1] * $scale, $p[2] * $scale ];
}

function TransformColour($colour, $scale)
{
    $hexToVal = [ 'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15 ];
    $valToHex = array_flip($hexToVal);
    $valToHex[0] = "0";

    // d/rgb
    // r, g, b are hex
    $parts = str_split($colour);
    if (count($parts) != 5) {
        return $colour;
    }
    // fullbright/unshaded
    if ($parts[0] === 'f') {
        return $colour;
    }

    $process = function($value) use ($hexToVal, $valToHex, $scale) {
        $value = isset($hexToVal[$value]) ? $hexToVal[$value] : (float) $value;
        $value = round($value * $scale);
        if ($value < 0) {
            $value = 0;
        }
        if ($value > 15) {
            $value = 15;
        }
        $value = isset($valToHex[$value]) ? $valToHex[$value] : $value;
        return $value;
    };

    $parts[2] = $process($parts[2]);
    $parts[3] = $process($parts[3]);
    $parts[4] = $process($parts[4]);

    return implode('', $parts);
}

function ProcessPlane($line)
{
    $parts = preg_split('/\\s+/', $line);
    if (count($parts) < 28) {
        return $line;
    }
    if ($parts[0] != '('
        || ! is_numeric($parts[1])
        || ! is_numeric($parts[2])
        || ! is_numeric($parts[3])
        || $parts[4] != ')'
        || $parts[5] != '('
        || ! is_numeric($parts[6])
        || ! is_numeric($parts[7])
        || ! is_numeric($parts[8])
        || $parts[9] != ')'
        || $parts[10] != '('
        || ! is_numeric($parts[11])
        || ! is_numeric($parts[12])
        || ! is_numeric($parts[13])
        || $parts[14] != ')'
    ) {
        return $line;
    }

    $p1 = [ $parts[1], $parts[2], $parts[3] ];
    $p2 = [ $parts[6], $parts[7], $parts[8] ];
    $p3 = [ $parts[11], $parts[12], $parts[13] ];

    $cross = crossproduct(subtract($p2, $p1), subtract($p3, $p2));
    $cross = normalize($cross);

    $dot = dotproduct($cross, normalize([ 0, -1, -2 ]));
    $dot = 0.75 + $dot * 0.25;

    $colour = $parts[27];
    $colour = TransformColour($colour, $dot);
    $parts[27] = $colour;

    return implode(' ', $parts) . "\n";
}

$fp = fopen($argv[2], 'w');
foreach (file($argv[1]) as $line) {
    $line = ProcessPlane($line);
    fwrite($fp, $line);
}
fclose($fp);

